package no.thorsenhig.jan_henrik.drawingapp_ass2;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;

import java.io.File;
import java.io.FileOutputStream;
import java.util.Random;
import java.util.UUID;
import android.provider.MediaStore;
import android.app.AlertDialog;
import android.app.Dialog;
import android.view.View.OnClickListener;
import android.widget.ShareActionProvider;
import android.widget.TabHost;
import android.widget.TextView;
import android.widget.Toast;


//Code from other sources (Has been added to, and edited):

    /*-Main drawing app tutorial-
    http://code.tutsplus.com/tutorials/android-sdk-create-a-drawing-app-touch-interaction--mobile-19202*/

    /*-Accelerometer-
    http://developer.android.com/guide/topics/sensors/sensors_motion.html
    http://scr.csc.noctrl.edu/courses/csc490MobileApps/overheads/ShakerTutorial.pdf

   /* -Save image function-
    http://stackoverflow.com/questions/7887078/android-saving-file-to-external-storage*/

   /*-Sharing function-
    http://www.raywenderlich.com/56109/make-first-android-app-part-2 */




public class DrawingActivity extends Activity implements OnClickListener, SensorEventListener {


    private ImageView mImageView; //Imageview to display last
    private DrawingView drawView; //Instance of the custom drawview
    private ImageButton currentPaint, drawButton, eraseBtn, saveBtn; //Buttons on header
    private float smallBrush, mediumBrush, largeBrush; //Brush sizes
    private SensorManager mSensorManager; //Sensormanager for the use of the accelerometer
    private Sensor mAccelerometer;  //Instance of device sensor for the use of the accelerometer
    private String disp;            //Name of image file
    TextView title;                 //Textview for shaking device
    private float mLastX, mLastY, mLastZ;   //Floats for registering movement
    ShareActionProvider mShareActionProvider; //Action provider for the share functionality


    private boolean newDialogExist = false;

    private final float NOISE = (float)2.0;

    Bitmap bit;

    //Folder where we'll save the images
    String tryFolder = Environment.getExternalStorageDirectory().toString()+"/saved_images/";


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drawing);
        mImageView = (ImageView) findViewById(R.id.start);
        drawView = (DrawingView)findViewById(R.id.drawing);

        LinearLayout paintLayout = (LinearLayout)findViewById(R.id.paint_colors);
        currentPaint = (ImageButton)paintLayout.getChildAt(0);
        currentPaint.setImageDrawable(getResources().getDrawable(R.drawable.paint_pressed));

        smallBrush = getResources().getInteger(R.integer.small_size);
        mediumBrush = getResources().getInteger(R.integer.medium_size);
        largeBrush = getResources().getInteger(R.integer.large_size);

        drawButton = (ImageButton)findViewById(R.id.draw_btn);
        drawButton.setOnClickListener(this);

        eraseBtn = (ImageButton)findViewById(R.id.erase_btn);
        eraseBtn.setOnClickListener(this);

        saveBtn = (ImageButton)findViewById(R.id.save_btn);
        saveBtn.setOnClickListener(this);



        //get the sensor service
        mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        //get the accelerometer sensor
        mAccelerometer = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);


        title=(TextView)findViewById(R.id.name);


        //Setup for the tabs used
        TabHost tabHost = (TabHost) findViewById(R.id.tabHost);
        tabHost.setup();

        TabHost.TabSpec tabSpec = tabHost.newTabSpec("drawing");
        tabSpec.setContent(R.id.tabDrawing);
        tabSpec.setIndicator(getResources().getString(R.string.drawing_screen));
        tabHost.addTab(tabSpec);

        tabSpec = tabHost.newTabSpec("display");
        tabSpec.setContent(R.id.tabDisplay);
        tabSpec.setIndicator(getResources().getString(R.string.display_last_drawing));
        tabHost.addTab(tabSpec);


        title.setText(R.string.shake_device);



    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.share, menu);

        // Access the Share Item defined in menu XML
        MenuItem shareItem = menu.findItem(R.id.menu_item_share);

        // Access the object responsible for
        // putting together the sharing submenu
        if (shareItem != null) {
            mShareActionProvider = (ShareActionProvider)shareItem.getActionProvider();
        }

        return true;
    }

    private void setShareIntent() {

        if (mShareActionProvider != null) {

            drawView.setDrawingCacheEnabled(true);

            String imgSaved = MediaStore.Images.Media.insertImage(
                    getContentResolver(), drawView.getDrawingCache(),
                    UUID.randomUUID().toString()+".png", "drawing");
            Uri uri = Uri.parse(imgSaved);

             /*
            Making use on the original save function getting url from last saved
            picture but will not share with facebook and such apps as the picture
            is only recognised as a file, not a picture. But will work with Skype.

            String path = tryFolder + disp;
            Log.i("Drawingapp", path);
            Uri uri = Uri.parse(path);
            */

            // create an Intent with the contents
            Intent shareIntent = new Intent(Intent.ACTION_SEND);

            shareIntent.setType("image/*");
            shareIntent.putExtra(Intent.EXTRA_STREAM, uri);

            // Make sure the provider knows
            // it should work with that Intent
            mShareActionProvider.setShareIntent(shareIntent);

            drawView.destroyDrawingCache();

        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    //User is drawing
    public void paintClicked(View view){
        drawView.setErase(false);
        drawView.setBrushSize(drawView.getLastBrushSize());
        //use chosen color
        if(view!=currentPaint){
            //update color
            ImageButton imgView = (ImageButton)view;
            String color = view.getTag().toString();
            drawView.setColor(color);
            imgView.setImageDrawable(getResources().getDrawable(R.drawable.paint_pressed));
            currentPaint.setImageDrawable(getResources().getDrawable(R.drawable.paint));
            currentPaint=(ImageButton)view;
        }
    }

    @Override
    public void onClick(View view) {

        //draw button clicked
        if(view.getId()==R.id.draw_btn){

            final Dialog brushDialog = new Dialog(this);
            brushDialog.setTitle(R.string.brush_size);
            brushDialog.setContentView(R.layout.brush_chooser);
            drawView.setBrushSize(mediumBrush);

            //User chooses small brush
            ImageButton smallBtn = (ImageButton)brushDialog.findViewById(R.id.small_brush);
            smallBtn.setOnClickListener(new OnClickListener(){
                @Override
                public void onClick(View v) {

                    drawView.setBrushSize(smallBrush);
                    drawView.setLastBrushSize(smallBrush);
                    drawView.setErase(false);
                    brushDialog.dismiss();
                }
            });

            //User chooses medium brush
            ImageButton mediumBtn = (ImageButton)brushDialog.findViewById(R.id.medium_brush);
            mediumBtn.setOnClickListener(new OnClickListener(){
                @Override
                public void onClick(View v) {

                    drawView.setBrushSize(mediumBrush);
                    drawView.setLastBrushSize(mediumBrush);
                    drawView.setErase(false);
                    brushDialog.dismiss();
                }
            });


            //User chooses large brush
            ImageButton largeBtn = (ImageButton)brushDialog.findViewById(R.id.large_brush);
            largeBtn.setOnClickListener(new OnClickListener(){
                @Override
                public void onClick(View v) {

                    drawView.setBrushSize(largeBrush);
                    drawView.setLastBrushSize(largeBrush);
                    drawView.setErase(false);;
                    brushDialog.dismiss();
                }
            });

            brushDialog.show();


            //erase button clicked
        } else if(view.getId()==R.id.erase_btn){
            //switch to erase - choose size
            final Dialog brushDialog = new Dialog(this);
            brushDialog.setTitle(R.string.eraser_size);
            brushDialog.setContentView(R.layout.brush_chooser);

            ImageButton smallBtn = (ImageButton)brushDialog.findViewById(R.id.small_brush);
            smallBtn.setOnClickListener(new OnClickListener(){
                @Override
                public void onClick(View v) {
                    drawView.setErase(true);
                    drawView.setBrushSize(smallBrush);
                    brushDialog.dismiss();
                }
            });
            ImageButton mediumBtn = (ImageButton)brushDialog.findViewById(R.id.medium_brush);
            mediumBtn.setOnClickListener(new OnClickListener(){
                @Override
                public void onClick(View v) {
                    drawView.setErase(true);
                    drawView.setBrushSize(mediumBrush);
                    brushDialog.dismiss();
                }
            });
            ImageButton largeBtn = (ImageButton)brushDialog.findViewById(R.id.large_brush);
            largeBtn.setOnClickListener(new OnClickListener(){
                @Override
                public void onClick(View v) {
                    drawView.setErase(true);
                    drawView.setBrushSize(largeBrush);
                    brushDialog.dismiss();
                }
            });

            brushDialog.show();


            //save button clicked
        } else if(view.getId()==R.id.save_btn){

            //save drawing
            AlertDialog.Builder saveDialog = new AlertDialog.Builder(this);
            saveDialog.setTitle(R.string.save_image);
            saveDialog.setMessage(R.string.save_image);
            saveDialog.setPositiveButton(R.string.confirm, new DialogInterface.OnClickListener(){
                public void onClick(DialogInterface dialog, int which){
                    //Enable drawing cache
                    drawView.setDrawingCacheEnabled(true);


                    //save to storage function, will return name of image
                    disp = SaveImage(drawView.getDrawingCache());


                    String imgLocation = tryFolder + disp;


                    bit = BitmapFactory.decodeFile(imgLocation);

                    mImageView.setImageBitmap(bit);

                    setShareIntent();


                    //Show success message!
                    if(disp!=null){
                        Toast savedToast = Toast.makeText(getApplicationContext(),
                                R.string.image_saved, Toast.LENGTH_SHORT);
                        savedToast.show();



                    }
                    else{
                        //Show error message!
                        Toast unsavedToast = Toast.makeText(getApplicationContext(),
                                R.string.image_not_saved, Toast.LENGTH_SHORT);
                        unsavedToast.show();
                    }

                    //Destroy drawing cache (VERY IMPORTANT!)
                    drawView.destroyDrawingCache();
                }
            });
            saveDialog.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener(){

                public void onClick(DialogInterface dialog, int which){
                    dialog.cancel();


                }
            });
            saveDialog.show();
        }


    }

    @Override
    public final void onAccuracyChanged(Sensor sensor, int accuracy)
    {

    }
    @Override
    //When the device is being shaken
    public final void onSensorChanged(SensorEvent event)
    {

        //Values for directions
        float x = event.values[0];
        float y = event.values[1];
        float z = event.values[2];
        //Delta-directions
        float deltaX = Math.abs(mLastX - x);
        float deltaY = Math.abs(mLastY - y);
        float deltaZ = Math.abs(mLastZ - z);


        //Check deltas against constant
        //Determine which level the device is being moved on
        if (deltaX < NOISE) {
            deltaX = (float) 0.0;
        }

        if (deltaY < NOISE) {
            deltaY = (float) 0.0;
        }

        if (deltaZ < NOISE) {
            deltaZ = (float) 0.0;
        }

        mLastX = x;
        mLastY = y;
        mLastZ = z;

        float minumimRef = 10.0f;



        //If accelerometer registers movement, ask user to start new draewing
        if(deltaX > minumimRef || deltaY > minumimRef || deltaZ > minumimRef ) {
            final AlertDialog.Builder newDialog = new AlertDialog.Builder(this);
            newDialog.setTitle(R.string.start_new);
            newDialog.setMessage(R.string.shake_confirm);
            newDialog.setPositiveButton(R.string.confirm, new DialogInterface.OnClickListener(){
                public void onClick(DialogInterface dialog, int which){
                    newDialogExist = false;
                    drawView.startNew();
                    dialog.dismiss();
                }
            });
            newDialog.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener(){
                public void onClick(DialogInterface dialog, int which){
                    newDialogExist = false;
                    dialog.cancel();
                }
            });

            if(newDialogExist == false) {
                newDialogExist = true;
                newDialog.show();
            }


        }

    }

    @Override
    protected void onResume()
    {
        super.onResume();
        mSensorManager.registerListener(this, mAccelerometer, SensorManager.SENSOR_DELAY_NORMAL);
    }
    @Override
    protected void onPause()
    {
        super.onPause();
        mSensorManager.unregisterListener(this);
    }


    //Take bitmap, in this case the drawingcache as a parameter
    //Save image
    private String SaveImage(Bitmap finalBitmap) {

        //Set folder for saving
        String root = Environment.getExternalStorageDirectory().toString();
        File myDir = new File(root + "/saved_images");
        myDir.mkdirs();

        //Set random image name
        Random generator = new Random();
        int n = 10000;
        n = generator.nextInt(n);
        String fname = "Image-"+ n +".jpg";


        //Save file
        File file = new File (myDir, fname);
        if (file.exists ()) file.delete ();
        try {
            FileOutputStream out = new FileOutputStream(file);
            finalBitmap.compress(Bitmap.CompressFormat.JPEG, 90, out);
            out.flush();
            out.close();

        } catch (Exception e) {
            e.printStackTrace();
        }


        //Return name of image
        return fname;
    }
}
